# Source code to correct circumferential and longitudinal motion distortion in catheter-based endoscopic OCT
## Overview
This repository contains the source code to correct the motion distortion described in the paper *"Correction of circumferential
and longitudinal motion distortion in high-speed catheter/endoscope-based Optical Coherence Tomogray"* by Nguyen et al.

## Installation requirements
- MATLAB (version >= 2019a) with the Image Processing Toolbox installed.

## Code organization
There are two entries points in the code, corresponding to the file `demo_enface_only.m` and `demoWholeStackCorrection.m`. Other files are supporting functions. See their docstring for more information.

The first entry file implements the whole processing pipeline using NURD-corrected data as input.
The processing pipeline consists of:
1. Find the optimal en face image for motion estimation.
2. Estimade the circumferential motion, correct for it. Save the output in tiff format.
3. Use the image that was corrected for circumferential motion to estimate the longitudinal motion, correct for it. Save the result
iin tiff format.
The second one does not estimate the motion. It uses motion estimated from `demo_enface_only.m` to resample all en face images in the stack.

## Data to run the source code
Because the images shown in this paper are clinical images obtained from the VA Boston Healthcare System, certain restrictions may be applied regarding the share policy of data. Please contact the corresponding author if you need the data to try the source code. It may take a while for us to get back to you, we need to check on these policy.

## Running the code

- Obtain the data to run the source code.
- Change the path to the data folder and the file name correctly in L62 of `demo_enface_only.m`. Then, hit the RUN button in Matlab to run through the code. The code should be pretty straight forward to understand. Please contact thnguyn@mit.edu if you have any questions.
Thanks for your interest in trying this code.

## Reference
[TBA]
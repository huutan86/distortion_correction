function [medDl, newBgVal] = estimateLongitudinalPullBack(...
    enfaceIm, params, bgVal)
    % Estimate the longitudinal pullback distortion using the power
    % spectrum.
    % Inputs: 
    %   enfaceIm: a shear corrected image of size [nFrames x nColsPerFrames] 
    %   mask: a binary map where we have the shear estimation.
    %   prevlCoord: previous l-coordinates that was being used to generate
    %   the current enface.
    % Outputs:
    %   medDl: the estimated longitudinal coordinate change 
    
    % Avoid the case where the stdVal can go big just because the sharp
    % transition due to motion correction. This is done by
    % repeating the entries on the left and the right image if it
    % has zero.
    dispDebugImages = true;
    newBgVal = bgVal;
    nFrames = size(enfaceIm, 1);
    nCols = size(enfaceIm, 2);

    % Calculate the enface image mask
    enfaceIm = normalizeIm(enfaceIm);
    mask = calculateDepaddingMask(enfaceIm);

    ws = round(params.windowSize / 2.0) * 2.0;
    stdValFull = zeros(nFrames, nCols);

    % Prepare a filter to filter in the circumferential dimension
    hCirc = fspecial('gaussian', round(3 * params.circStdVal), ...
        params.circStdVal); 
    % The standard deviation along the circumferential diemsntion that we use to estimate the pullback stretching and compressin g
    hCirc = sum(hCirc, 1);
    enfaceImFilt = imfilter(enfaceIm, hCirc, 'same');

    for rowIdx = ws / 2  + 1: nFrames - ws / 2
        firstRowIdx = max(rowIdx - ws / 2, 1);
        lastRowIdx = min(rowIdx + ws / 2, nFrames);
        imPatch = enfaceImFilt(firstRowIdx : lastRowIdx, :);
        stdValFull(rowIdx, :) = calculateStdVal(imPatch);
    end


    if (isempty(bgVal))
        % Ask the user to select a reference region if we don't have
        % a previous estimate or specifically told to do so.
        ws_bg = 40;
        % Ask the user to select a background region
        disp('Please pick bright uncontact region..');
        [x_bg, y_bg] = ginput(1);
        x_bg = round(x_bg);
        y_bg = round(y_bg);
        xleft_bg = round(x_bg - ws_bg / 2+ 1);
        ytop_bg = round(y_bg - ws_bg / 2+ 1);
        xright_bg = round(x_bg + ws_bg / 2);
        ybot_bg = round(y_bg + ws_bg / 2);
        temp = enfaceIm .* mask;
        bgVal = mean(mean(temp(ytop_bg : ...
            ybot_bg, xleft_bg : xright_bg)));
        newBgVal = bgVal;
    end

    nonBgMask = enfaceIm .* mask > (1.1 * bgVal);
    nonBgMask = imopen(nonBgMask, strel('disk', 10));
    nonBgMask = bwareaopen(nonBgMask, 20000);

    if (dispDebugImages)
        figure(4);
        imagesc(normalizeIm(stdValFull .* ...
            nonBgMask .* calculateDepaddingMask(enfaceIm)), [0 0.2]);
        colormap gray;
        title('Standard deviation mask')
    end

   % Compute the median of the std along the circumferential dimension
    maskedStdIm = stdValFull .* nonBgMask .* ...
       calculateDepaddingMask(enfaceIm);

    medStdVal = zeros(1, nFrames);
    for rowIdx = 1: nFrames
        curRow = maskedStdIm(rowIdx, :);
        curRow = curRow(curRow > 0);
        medStdVal(rowIdx) = median(curRow);
    end

    % Remove any nan values
    nonNanIdx = find(~isnan(medStdVal));
    medStdVal(1: nonNanIdx(1) -1) = medStdVal(nonNanIdx(1));
    medStdVal(nonNanIdx(end) + 1 : end) = medStdVal(nonNanIdx(end));

    % Compute the relative std between a region to its surrounding
    normalizedStdVal = zeros(1, nFrames);
    regionWidth = 100;
    for rowIdx = 1 : nFrames
        firstRow = max(1, rowIdx - regionWidth / 2);
        lastRow = min(nFrames, rowIdx + regionWidth / 2);
        normalizedStdVal(rowIdx) = medStdVal(rowIdx) / ...
            mean(medStdVal(firstRow : lastRow));
    end

    for rowIdx = 1 : nFrames
        firstRow = max(1, rowIdx - regionWidth / 2);
        lastRow = min(nFrames, rowIdx + regionWidth / 2);
        normalizedStdVal(rowIdx) = medStdVal(rowIdx) / ...
            mean(medStdVal(firstRow : lastRow));
    end

    % For the top and the bottom, we associate it with the std of the
    % reference to avoid scaling these areas
    normalizedStdVal(1 : ws / 2) = normalizedStdVal(ws / 2 + 1);
    normalizedStdVal(nFrames - ws / 2 + 1 : end) = ...
        normalizedStdVal(nFrames - ws / 2);
    % Large value for std => Faster speed => larger dl
    % Use a nonlinear mapping to correct more for the small dl.
    medDl = normalizedStdVal;
    medDl = smooth(medDl, 5);
    medDl(medDl > 1.2) = 1.2;
    medDl(medDl < 0.8) = 0.8;
    frameIdx = 1 : nFrames;
    if (dispDebugImages)
        figure(2);
        subplot(211);
        plot(frameIdx, medStdVal, 'linewidth', 2);
        xlabel('Time index t');
        ylabel('{\sigma}_{med}(t)')
        set(gca,'FontSize', 15)
        set(gcf,'color','w');
        grid on;
        axis([1 nFrames min(medStdVal) max(medStdVal)])
  
        %subplot(312);
        %plot(frameIdx, normalizedStdVal); 
        %title('Normlized std');
        subplot(212);
        plot(frameIdx, medDl, 'linewidth', 2); 
        xlabel('Time index t');
        ylabel('{\Delta}l(t)');
        set(gca,'FontSize', 15)
        set(gcf,'color','w');
        grid on;
        axis([1 nFrames 0.75 1.25])
    end
    %save('longitudinal_profile.mat', 'medStdVal', 'normalizedStdVal', 'medDl','frameIdx')
end


function stdVect = calculateStdVal(im)
    % Compute the standard deviation of the power spectrum along
    % the longitudinal dimension.
    imf = fft(im, [], 1);
    psdVal = abs(imf) .^ 2;
    stdVect = stdCal(psdVal); 
end

function [stdVal] = stdCal(y)
%   Calculate the std of the spectral density correlation from the power
%   spectral density distribution
%   assuming each column of y is a distribution.
%   Returns stdVal, the standard deviation of the power spectrum
    n = size(y, 1);
    midRow = ceil((n + 1) / 2);
    nCols = size(y, 2);
    xx = 1 : n;
    xx = xx - midRow;
    xx = ifftshift(xx);
    xx = repmat(xx(:), [1 nCols]);
    stdVal = sqrt(sum(y .* (xx .^ 2), 1)) ./ (max(y, [], 1) + eps);
end
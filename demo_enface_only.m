%% Demo source code to correct for motion for the micromotor probe
clc;
%clear all;
close all;

dataLoc = 'G:\My Drive\Papers\DistortionCorrection\!Structural Reading 2016\Validation_Sets\';  % Data location
useSimulateIm = 0;  % 0: simulated data, 1: clinical data.
%%  Samples to use:
% c2 - select depth = 97 
% c3 - select depth = 82
% c4 - select depth = 76
% c6 - select depth = 94
% c7 - select depth = 87
% c8 - select depth = 89
% c9 - select depth = 88
% c10 - select depth = 115
% c12 - select depth = 93
% c13 - select depth = 66
% c14 - select depth = 102
% c17 - select depth = 129
% c18 - select depth = 117
% c19 - select depth = 96
% c21 - select depth = 120
% c22 - select depth = 102
% c23 - select depth = 100
% c24 - select depth = 112  
% c27 - select depth = 107
% c29 - select depth = 127
% c31 - select depth = 95
% c32 - select depth - 101
% c33 - select depth = 87
% c36 - select depth = 128
% c37 - select depth = 97
% c38 - select depth = 113
% c39 - select depth = 94
% c41 - select depth = 114
% c42 - select depth = 68
% c43 - select depth = 106
% c46 - select depth = 128
% c48 - select depth = 143 - don't use - middle area is corrupted.
% c49 - select depth = 116 
% c50 - select depth = 94
% c51 - select depth = 101
% c54 - select depth = 101 - don't use dark band, no contact. 
% c56 - select depth = 138
% c58 - select depth = 116
% c60 - select depth = 84;
% c62 - select depth = 93;
% c63 - select depth = 95;
% c64 - select depth = 84
% c65 - select depth = 77
% c66 - select depth = 153
% c67 - select depth = 126
% c68 - select depth = 159
% c69 - select depth = 132
% c70 - select depth = 96
% c71 - select depth = ?
% c72 - select depth = 100
% c73 - select depth = 100

useSavedOptimalEnface = 1;   % if 1: an optimal en face image will be selected from the stack, if 0, use a preselected one.
dataSet = 'c27';
selectedEnfaceDepth = 107;
%% Create output folders, create them if not yet existed.
outputFolder = strcat(dataLoc, dataSet);
correctedFolder = strcat(outputFolder, '\corrected\')
if (~exist(correctedFolder))
    mkdir(correctedFolder);
end

%% Parameters to select an area not obstructed by strut.
firstCircCol = 200;  % First circumferential column
lastCircCol  = 800;  % Last circumferential column

%% Parameters to define the longitudinal range for processing 
firstFrame = 1;

%% Main processing
disp(['Processing ' num2str(dataSet) '...']);

%% Step 1: pre-processing
% Select the optimal en face if not specified.
if (useSavedOptimalEnface)
    % Load the optimal en face and its parameter.
    enfaceIm = single(imread(strcat(correctedFolder, 'optimal_enface.tif')));
    load(strcat(correctedFolder, '\OptimalEnfaceDepth.mat'), 'enFaceDepth');  
else
    disp([' Loading the whole stack to find the optimal enface...']);
    fileName=strcat(dataLoc, dataSet, '\!LogIntensityAll.tif');
    InfoImage=imfinfo(fileName);
    numCols=InfoImage(1).Width;
    numRows=InfoImage(1).Height;
    numFrames=length(InfoImage);
    imStack=zeros(numRows, numCols ,numFrames,'single');  % Enface stacks.
    TifLink = Tiff(fileName, 'r');
    for frameIdx=1:numFrames
       if (mod(frameIdx, 100)==0) 
           disp(['Reading frame ' num2str(frameIdx) '/' num2str(numFrames)]);
       end
       TifLink.setDirectory(frameIdx);
       imStack(:,:,frameIdx)=single(TifLink.read());
    end
    TifLink.close();
    
    %% Search for the optical enface.
    startDepth = 80;  % Only search within the tissue region.
    if (isempty(selectedEnfaceDepth))
        enFaceDepth = selectOptimalDepth(imStack, startDepth);
    else
        enFaceDepth = selectedEnfaceDepth;
    end
    disp(['Optimal enface is at depth: ' num2str(enFaceDepth)]);
    save(strcat(correctedFolder, '\OptimalEnfaceDepth.mat'), 'enFaceDepth');
    
    %% Use the estimated depth to generate the optimal enface
    enfaceIm = squeeze(mean(imStack(enFaceDepth - 5: enFaceDepth + 5, :, :),1));
    enfaceIm = enfaceIm';   % Vertical = longitudinal.
    enfaceIm = enfaceIm(:,firstCircCol : lastCircCol);  % Get rid of the strut
    writeTIFF(enfaceIm, strcat(correctedFolder, 'optimal_enface.tif'));
end

%% Step 2: circumferential motion estimation & correction
% Parameter for enface correction.
nFlowXDisp = 20;  % Number of points that we used to display the flow in the X-dimension
nFlowZDisp = 120;
scaleFlowX = 40;
scaleFlowY = 5;
stripSize = 3;
params.numPoints    = [stripSize 20];  % Number of points to estimate in the time and circumferential dimensions.
params.stdTensor    = 10.0;    % Standard deviation for velocity estimation in the circumferential dimension.
params.stdTensorz   = 1;       % Standard deviation for velocity estimation in the longitudinal imension.
params.normGamma 	= 0.001;   % Normalization for the gradient.
params.m            = 0.01 * max(enfaceIm(:));     % Maximum pixel value
params.tcNorm       = 1e-4;    % Tikhonov normalization factor to suppress the artifacts of noise.
params.maxNumIter   = 10;       % Optical estimation 20 times.
params.dsScaleFactor  = 1;

% Parameters for the correction.
nFrames = size(enfaceIm, 1);
lastFrame = nFrames;

% Determine whether we should correct for the shear distortion or not
%h = fspecial('gaussian', [15 4]);
%enfaceIm = imfilter(enfaceIm, h, 'same');   % Kill all the high frequency in the image
Ik_minus_1 = [];

% Back up the enface image before continuing
orgEnfaceIm = enfaceIm;
% Display the raw en fance image.
figure(1);
minEnfaceValue = min(orgEnfaceIm(:));
maxEnfaceValue = max(orgEnfaceIm(:));
imshow(normalizeIm(orgEnfaceIm)); 
changeLutToLightLab(); 
truesize;
axis off;
ylabel('Longitudinal');
xlabel('Circumference');
title('Raw enface image');

numRepeats = 30;
estimateDeltaC = false;
loadPrecorrectedEnface = false;
deltaCs = zeros(nFrames - 1, params.numPoints(2), numRepeats);
cCoordAllRepeats = cell(numRepeats, 1);  % Coordinates of the pixels from all
% repeats. This is used to correct all enface afterwards.
if estimateDeltaC
    for repeatIdx = 1 : numRepeats
        nCols = size(enfaceIm, 2);
        for tIdx = (stripSize + 1)/ 2 : lastFrame - (stripSize + 1)/ 2 - 1
            if (mod(tIdx, 100) == 0)
                disp(['Estimating deltaC at time ' num2str(tIdx)])
            end
            Ik_c_l = enfaceIm(tIdx - (stripSize - 1) / 2 : tIdx + (stripSize - 1)/ 2, :);  % Generate the current portion of the enface.
            Ik_cp1_lp1 = enfaceIm(tIdx + 1 - (stripSize - 1) / 2 : tIdx +(stripSize + 1)/ 2, :);
            deltaC = opticalFlowEnface2(Ik_cp1_lp1, Ik_c_l, params);
            if (~isempty(deltaC))
                deltaCs(tIdx, :, repeatIdx) = max(deltaC(:, :), [], 1);  % Shear velocity - select the max across all the time index
            end
        end
        
        % Just display deltaC in the first iteration - for the paper
        if (repeatIdx == 1)
            % Resamples the low resolution deltaCs to full resolution
            deltaCs2 = resampleDeltaCToFullRes(deltaCs(:,:,1), nFrames, nCols);
            % Display the estimation of circumfernetial coordinate shift.
            figure(2)
            imshow(normalizeIm(deltaCs2));
            colormap(whitejet);
            title('DeltaCs')
        end

        %% Step 3: Correct for the circumferential distortion
        cCoord = estimateCCoord(nFrames, nCols, deltaCs(:,:,repeatIdx)); % Add constraints to calculate C            
        corrEnface = resampleEnfaceX(enfaceIm, cCoord);
        cCoordAllRepeats{repeatIdx} = cCoord;

        figure(3);
        imshow(normalizeIm(corrEnface .* calculateDepaddingMask(corrEnface), minEnfaceValue, maxEnfaceValue)); 
        changeLutToLightLab(); 
        truesize; axis off;
        ylabel('Longitudinal'); xlabel('Circumference');
        title(strcat('Corrected image at repeat ', num2str(repeatIdx)));
        %Prepare for the next iteration

        enfaceIm = corrEnface;
        writeTIFF(enfaceIm, strcat(correctedFolder, 'enfaceIm_', num2str(repeatIdx),'.tif'));
        save(strcat(correctedFolder, ...
            'circumferential_distortion_params.mat'), 'cCoordAllRepeats');
    end
else
    % Don't do the estimation for circumferential distortion, use the
    % pre-estimated instead.
    if (~loadPrecorrectedEnface) 
        disp('Loading estimated coordinates for resampling...');
        tic;
        load(strcat(correctedFolder, 'circumferential_distortion_params.mat'));
        toc;
        % Resample the input en face as many time as defined in the cCoord
        % array.
        for repeatIdx = 1 : numRepeats
            enfaceIm = resampleEnfaceX(enfaceIm, cCoordAllRepeats{repeatIdx});
            mask = calculateDepaddingMask(enfaceIm);
            enfaceImMasked = enfaceIm .* mask;
            writeTIFF(enfaceImMasked, strcat(correctedFolder, 'enfaceIm_',...
            num2str(repeatIdx),'.tif'))
        end
         
    else
        % Use the pre-saved corrected enface.
        enfaceIm = single(imread(strcat(correctedFolder, 'enfaceIm_',...
            num2str(numRepeats),'.tif')));
    end
    mask = calculateDepaddingMask(enfaceIm);
    corrEnface = enfaceIm;
    figure(3);
    enfaceImMasked = enfaceIm .* mask;
    imshow(normalizeIm(enfaceImMasked, minEnfaceValue, maxEnfaceValue)); 
    changeLutToLightLab(); 
    truesize; axis off;
    ylabel('Longitudinal'); xlabel('Circumference');
    title(strcat('Corrected en face '));
   
end

%% Step 4: Steplongitudinal motion estimation & correction
estimatePullBack = true;
corrEnface = maskEmptyRegion(corrEnface);
mask = calculateDepaddingMask(corrEnface);
figure(3);
imshow(normalizeIm(mask .* corrEnface, minEnfaceValue, maxEnfaceValue)); 
changeLutToLightLab(); 
truesize; axis off;
ylabel('Longitudinal'); xlabel('Circumference');
title(strcat('Corrected en face '));


if estimatePullBack
    longPullbackEstPrams.circStdVal = 5;
    longPullbackEstPrams.windowSize = 3;  % Window size to estimate the pullback speed
    %% Generate a mask to mask out squam and out of contact areas
    numLongCorrRepeat = 300;
    bgVal = [];  % The estimated l-coordinates that was used during the resampling.
    metrics = zeros(numLongCorrRepeat, 1);
    lCoordAllRepeats = cell(0, 1);  % Coordinates of the pixels from all
    for repeatIdx = 1 : numLongCorrRepeat
        mask = calculateDepaddingMask(corrEnface);
        [medianDl, bgVal] = estimateLongitudinalPullBack(...
            corrEnface, longPullbackEstPrams, bgVal);
        tenPct = prctile(medianDl, 10);
        ninetyPct = prctile(medianDl, 90);
        pctDiff = ninetyPct - tenPct;
        metrics(repeatIdx) = pctDiff;
        disp(['Iter = ' num2str(repeatIdx) ', 90 pct - 10 pct of Dl=' ...
            num2str(pctDiff)]);
        figure(5);
        plot(1 : repeatIdx, metrics(1 : repeatIdx));
        xlabel('Iterations'); ylabel('Metrics');
        lCord = cumsum(medianDl);
        corrEnface = resampleEnfaceY(corrEnface, lCord);
        lCoordAllRepeats{end + 1} = lCord;
        if (pctDiff <= 0.03)
            break;
        end
     
    end
    imMask = calculateDepaddingMask(corrEnface);
    save(strcat(correctedFolder, 'longitudinal_distortion_params.mat'), ...
        'lCoordAllRepeats', 'imMask');
else
    load(strcat(correctedFolder, 'longitudinal_distortion_params.mat'), ...
        'lCoordAllRepeats');
    numRepeats = length(lCoordAllRepeats);
    for repeatIdx = 1 : numRepeats
        corrEnface = resampleEnfaceY(corrEnface, lCoordAllRepeats{repeatIdx});
    end
end
figure(7); hold off;
imshow(normalizeIm(corrEnface .* calculateDepaddingMask(corrEnface), ...
    minEnfaceValue, maxEnfaceValue)); 
changeLutToLightLab(); 
truesize; axis off;
ylabel('Longitudinal'); xlabel('Circumference');
title(strcat('Final corrected enface '));
writeTIFF(corrEnface .* calculateDepaddingMask(corrEnface), ...
    strcat(correctedFolder, 'final_image.tif'));
       

       

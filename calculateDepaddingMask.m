function shrinkMask = calculateDepaddingMask(enfaceIm)
% Calculate a mask to apply to remove the padding regions
    nRows = size(enfaceIm, 1);
    diffIm = abs(diff(enfaceIm, 1, 2));
    mask = (diffIm > 1e-4);
    mask = [zeros(nRows, 1) mask];
    mask = imfill(mask, 'hole');
    mask = imopen(mask, strel('disk', 5));
    shrinkMask = zeros(size(enfaceIm));
    bandSize = 16;
    for rowIdx = 1 : size(mask, 1)
        idx = find(mask(rowIdx, :));
        shrinkMask(rowIdx, idx(bandSize : end - bandSize)) = ...
            mask(rowIdx, idx(bandSize : end - bandSize));
    end
end
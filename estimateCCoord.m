function c = estimateCCoord(nFrames, nCols, dC)
    % Estimate the circumferential coordinates of the sampling points.
    nColsGrid = size(dC, 2);
    % Solve a quadratic programing problems to find the sampling locations
    disp('Fitting all velocity estimates to find locations...');
    cGrid = findSamplingLoc(-dC, nCols, 0.99, 1.01); % Note that the image feature direction is inverse to the direction of our capsule.
    % Meaning if we see a bar on the image to the right, our capsule
    % actually goes left.
        
    x = linspace(1, nCols, nCols);
    t = linspace(1, nFrames, nFrames);
    xGrid = linspace(1, nCols, nColsGrid);
    tGrid = linspace(1, nFrames, nFrames);
    [xxGrid, ttGrid] = meshgrid(xGrid, tGrid);
    [xx, tt] = meshgrid(x, t);
     
    % Upsampling the flow
    c = interp2(xxGrid, ttGrid, cGrid, xx, tt);
end

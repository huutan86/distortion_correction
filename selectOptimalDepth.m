function optDepth = selectOptimalDepth(imStack, minSearchRange)
    % Selects the enface image with the largest deviation of the pixel 
    % pixel values, starting from `minSearchRange`.
    nDepths = size(imStack, 1);
    stdArr = zeros(nDepths, 1);
    for depthIdx = 1 : nDepths
        curIm = single(squeeze(imStack(depthIdx, :, :)));  % Ex
        stdArr(depthIdx) = std(curIm(:));
    end
    [~, maxIdx] = max(stdArr(minSearchRange : end));
    optDepth = maxIdx + minSearchRange - 1;
end
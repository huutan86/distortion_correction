function changeLutToLightLab
    % This code change the LUT of matlab. In order to save the LUT from
    % imagej, go to Image -> Lookup table -> Select LUT.
    % Then, go to imagej -> Image -> Color -> Show LUTS -> List-> Export
    % the LUTs into CSV file
    lutVals = csvread('lightlab_lut.csv', 1, 1,[1, 1, 256, 3]);
    lutVals = lutVals / max(lutVals(:));
    colormap(lutVals);
end
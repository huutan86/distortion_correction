function [xref, yref, x_bg, y_bg] = selectReferenceAndBgRegions()
    % Select the background and the reference region.
    disp('Please pick a reference region');
    figure(3);
    [x, y] = ginput(1);
    xref = round(x);
    yref = round(y);
    
    ws = 20;
    % Plot a rectangle for the reference region.
    xleft = xref - ws / 2 + 1;
    ytop = yref - ws / 2 + 1;
    xright = xref + ws / 2;
    ybot = yref + ws / 2;
    w = xright - xleft;
    h = ybot - ytop;
    
    % Ask the user to select a background region
    disp('Please pick bright uncontact region..');
    [x_bg, y_bg] = ginput(1);
    x_bg = round(x_bg);
    y_bg = round(y_bg);
end
function imOut = cropEmptyRegion(imIn)
% Crop the zero pixels in the top or the bottom of the image
    meanOverCols = mean(imIn, 2);
    nonZeroRowIdx = find(meanOverCols > 0);
    imOut = imIn(nonZeroRowIdx, :);
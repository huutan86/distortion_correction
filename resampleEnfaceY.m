function corrEnface = resampleEnfaceY(enface, lCord)
    % Resample the current frame in the X-dimension.
    % Inputs:
    %   enface: input enface for correction enface for correction
    %   lCord: longitudinal coordinate of the sampling points
    % Outputs:
    %   corrEnface: corrected enface.
    nCols = size(enface, 2);
    minL = round(min(lCord));
    maxL = round(max(lCord));
    tCoordNew = linspace(minL, maxL, length(lCord));  % Keep the size the same.
    %tCoordNew = minL : maxL;
    newNFrames = length(tCoordNew);
    
    %% Calculate the new X coordinates after resampling. These coordinates are xx2 and yy2. Note that there will be points with these coordinate that we don't have info.
    corrEnface = zeros(newNFrames, nCols, 'single');
    for colIdx = 1 : nCols
       corrEnface(:, colIdx) = interp1(lCord', enface(:, colIdx)', tCoordNew', 'spline');
    end
    
end

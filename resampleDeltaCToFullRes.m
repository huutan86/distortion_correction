function outDeltaCs = resampleDeltaCToFullRes(deltaCs, newNRows, newCols)
% Resamples the input deltaC maps to the outputs.
nRows = size(deltaCs, 1);
nCols = size(deltaCs, 2);
[xx, yy] = meshgrid(1:nCols, 1:nRows);
[xxNew, yyNew] = meshgrid(linspace(1, nCols, newCols), ...
    linspace(1, nRows, newNRows));
outDeltaCs = interp2(xx, yy, deltaCs, xxNew, yyNew, 'spline');
end
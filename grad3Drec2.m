
function [Ac, Al, At] = grad3Drec2(Ik_cp1_lp1, Ik_c_lp1, Ik_c_l)
    % Inputs:
    %   Ik_cp1_lp1 = I[c(x,t + delta_t), l(x,t + delta_t)] 
    %   Ik_c_lp1 = I[c(x,t), l(x, t + delta_t)]
    %   Ik_c_l = I[c(x,t), l(x,t)].
    % Outputs:
    %   Ac, Al, At: gradients in the circumferential, longitudinal and time
    %   dimension.
    
    gg = [0.2163,   0.5674,   0.2163];
    % Compute the gradient in the circumferential dimension. We use our 
    % measurement to estimate it because we assume that the longitudinal
    % coordinate does not change.
    Ac = conv2(Ik_c_l(:,[2:end end ]) - Ik_c_l(:,[1 1:(end-1)]), gg','same');
     
    % Compute the gradient in the longitudinal dimension. This requires the
    % same coordinate x. Therefore, we need to use Ik_c_lp1.
    Al = conv2(Ik_c_lp1 - Ik_c_l, gg ,'same');
   
    
    % Compute the gradient in the time dimension.
    At = 2 * conv2(gg, gg, Ik_cp1_lp1 - Ik_c_l,'same');
end
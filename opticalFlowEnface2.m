function [deltaC] = opticalFlowEnface2(Ik_cp1_lp1, Ik_c_l, params)
    % Estimates dx from the data. See the manuscript for the algorithm.
    % Inputs:
    %   Ik_cp1_lp1 = I[c(x, t + delta_t), l(x, t + delta_t)]
    %persistent m200 m020 m110 m101 m011 ggx ggz Ac Al At;
    
    displayDeltaC = false;  % Display the estimation of dx.
    numPoints = params.numPoints;
    ggx  = single(gaussgen(params.stdTensor)); %%
    ggz = single(gaussgen(params.stdTensorz)); %%
    deltaC = zeros(numPoints, 'single');
    deletaCPrev = deltaC;
    prevError = 1;
    for iter = 1 : params.maxNumIter
        if (iter == 1)
            % First iteration, don't resample anything
            Ik_c_lp1 = Ik_c_l;  % I[c(x, t), l(x, t + delta_t)] = I[c(x, t), l(x, t)]
        else
            % Correct for the enface and use it to generate the
            % resampling.
            displayFlow = false;
            Ik_c_lp1 = resampleCurrentFrame(Ik_cp1_lp1, deltaC,displayFlow);
            if (displayDeltaC)
                figure(300);
                subplot(221);
                imagesc(Ik_cp1_lp1); title('Original frame');
                subplot(223);
                imagesc(Ik_c_lp1); title('Resampled frame');
                subplot(222);
                imagesc(deltaC); colorbar(); title('Current shift estimated');
                drawnow;
            end
        end
        % Calculate the difference in the circumferential dimension.
        [Ac, Al, At] = grad3Drec2(Ik_cp1_lp1, Ik_c_lp1, Ik_c_l);

            % Compute the normalization factor for gradient
        normFact = (Ac.^2 + Al.^2 + At.^2  + params.m * ...
            (params.normGamma)^2 + eps) / params.m;                
        Ac2 = (Ac .^ 2) ./ normFact;
        Al2 = (Al .^ 2) ./ normFact;
        Acdz = (Ac .* Al) ./ normFact;
        AcAt = (Ac .* At) ./ normFact;
        AlAt = (Al .* At) ./ normFact;

        % Combine with the aperture to increase the reliability.
        Ac2 = conv2(ggz, ggx, Ac2,'same');       % Use a waiting window for a more reliable estimation.
        Al2 = conv2(ggz, ggx, Al2, 'same'); 
        Acdz = conv2(ggz, ggx, Acdz,'same');     % Use a waiting window for a more reliable estimation.
        AcAt = conv2(ggz, ggx, AcAt,'same');     % Use a waiting window for a more reliable estimation.
        AlAt = conv2(ggz, ggx, AlAt,'same');     % Use a waiting window for a more reliable estimation.

        Ac2 = imresizeNN(Ac2 ,numPoints) + params.tcNorm;
        Al2 = imresizeNN(Al2 ,numPoints) + params.tcNorm;
        Acdz = imresizeNN(Acdz, numPoints);
        AcAt = imresizeNN(AcAt, numPoints);
        AlAt = imresizeNN(AlAt, numPoints);

        m200 = Ac2;
        m020 = Al2;
        m110 = Acdz;
        m101 = AcAt;
        m011 = AlAt;


        % Find the velocity at each point
        deltaC =(-m101 .* m020 + m011 .* m110)./(m020 .* m200 - m110 .^ 2); % Derivative in the column dimension
        relError = norm(deltaC(:) - deletaCPrev(:), 'fro') / (norm(deltaC(:),'fro') + 1e-3);
        if (displayDeltaC)
            disp(['Relative err = ' num2str(relError)]);
        end
        if ((relError > prevError) || (relError  < 1e-3))
            if (relError > prevError)
                % Recover the error if the current one goes bad.
                deltaC = deletaCPrev;
            end
        else
            deletaCPrev = deltaC;
        end
        prevError = relError;
    end 
    if (relError > 0.1)
        % Print out the relative error as sth may go wrong.
        disp(['Final relative err = ' num2str(relError)]);
    end
end

function [I_c_lp1, mask] = resampleCurrentFrame(I_cp1_lp1, dC_grid, displayFlow)
    % Resamples the I[c(x,t+dt), l(x, t+dt)] to generate I[c(x,t), l(x, t+dt)]
    if (isempty(displayFlow))
        displayFlow = false;
    end
    nT = size(I_cp1_lp1, 1);
    nC = size(I_cp1_lp1, 2);
    nC_grid = size(dC_grid, 2);
    cc_grid = repmat(single(linspace(1, nC, nC_grid)), [nT 1]);
    cc = repmat(single(1:nC), [nT 1]);
    isStrip = (nT>=2);
    if (isStrip)
        % Use 2D interpolation if we use a strip, not a single enface row.
        dC = zeros(nT, nC, 'single');
        cArr = single([1:nC]');
        for rowIdx = 1 : nT
            dC(rowIdx, :) = interp1(cc_grid(rowIdx,: ), dC_grid(rowIdx,:), ...
                cArr);
        end
        % clear all the isnan
        cc = cc - cumsum(dC, 1);
        % Generate an upsampled flow
        I_c_lp1 = zeros(nT, nC, 'single');
        for rowIdx = 1 : nT
            I_c_lp1(rowIdx, :) = interp1(cc(rowIdx,:), I_cp1_lp1(rowIdx,:),...
                cArr,'METHOD','extrap');
        end
        
        minCC = repmat(min(cc, [], 2), [1 nC]);
        maxCC = repmat(max(cc, [], 2), [1 nC]);
        
        % Display the flow on the top of the current image.
        ccDisp = cc_grid + cumsum(dC_grid, 1);
    else
        dC = interp1(cc_grid, dC_grid, cc, 'nearest');
        cc = cc - cumsum(dC, 2);
        minCC = repmat(min(cc, [], 2), [1 nC]);
        maxCC = repmat(max(cc, [], 2), [1 nC]);
        I_c_lp1 = interp1(cc, I_cp1_lp1, cc, 'nearest');
        
        % Display the flow on the top of the current image.
        ccDisp = cc_grid + cumsum(dC_grid, 2);
    end
   
    if (displayFlow)
        figure(400);
        imagesc(I_cp1_lp1'); hold on;
        plot(ccDisp, 'r');
        hold off;
        title('Current frame and estimated flow pre-corrected');
    end
    
    mask = (cc > minCC) .* (cc < maxCC);
    I_c_lp1 = I_c_lp1 .* single(mask);
end

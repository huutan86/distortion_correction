function [cGrid] = findSamplingLoc(deltaC, nCols, minFlowPct, maxFlowPct)
    % Solve for the location of the sampling points
    % Note that we need to solve the following set of equation:
    % xxFlowDistorted has dimension of [# frames, # flows]
    % x[m + 1, n] - x[m, n] = dx[m,n]
    % a<= x[m, n + 1] - x[m, n] <= b where a and b are calculated as 
    % a = minFlowPct * (nCols / number of flows of dx)
    % b = maxFlowPct * (nCols / number of flows of dx)
    % This problem can be converted into a linear programing problem
    % where
    % find x = arming ||Ax - b||^2 st. constraints.
    nFlowRows = size(deltaC, 1);
    nFlowCols = size(deltaC, 2);
    cFirstFrame = linspace(1, nCols, nFlowCols); % Coordinates of the flow at the first frame.
    spaceDiv = mean(diff(cFirstFrame));
    minDist = minFlowPct * spaceDiv;
    maxDist = maxFlowPct * spaceDiv;
    
    % Generate a variable vector x forming for differet frames as follows
    % [X] = [X1 X2 ... X_nFlowRows]^T where 
    % Xi = [x[i, 1] x[i 2] ... x[i nFlowCols]] ^ T is a column coordinate
    % vector for frame i. Note that X has a length of nFlowRows *
    % nFlowCols.
    
    %% Form a sparse matrix H to compute the difference
    nEle = nFlowRows * nFlowCols;   % Number of elements
    A = sparse(nEle, nEle);
    b = zeros(nEle, 1);
    
    % Generate the elements of H
    % Generate the +1 entries
    plus1RowIdx = 1 : nEle;
    plus1ColIdx = plus1RowIdx;    
    plus1Idx = sub2ind(size(A), plus1RowIdx, plus1ColIdx);
    A(plus1Idx) = 1;
    
    % Generate the -1 entries
    minus1RowIdx = nFlowCols + 1 : nEle;
    minus1ColIdx = 1 : nEle - nFlowCols;
    minus1Idx = sub2ind(size(A), minus1RowIdx, minus1ColIdx);
    A(minus1Idx) = -1;
   
    b(1 : nFlowCols) =  cFirstFrame(:) + deltaC(1, :)';
    dxT = deltaC(2:end, :)'; % Don't get the first frame.
    b(nFlowCols + 1: end) = dxT(:);
    
    Hqr = A' * A;
    fqr = -A' * b;
    clear A;
    clear b;
    
    % Generate the constraints matrix
    AsubMat = sparse(nFlowCols - 1, nFlowCols);
    plus1RowIdx = 1 : nFlowCols - 1;
    plus1ColIdx = 2 : nFlowCols;    
    plus1Idx = sub2ind(size(AsubMat), plus1RowIdx, plus1ColIdx);
    AsubMat(plus1Idx) = 1;
    
    minus1RowIdx = 1 : nFlowCols - 1;
    minus1ColIdx = 1 : nFlowCols - 1;
    minus1Idx = sub2ind(size(AsubMat), minus1RowIdx, minus1ColIdx);
    AsubMat(minus1Idx) = -1;
    Acstr = sparse((nFlowCols - 1)*nFlowRows, nEle);
    for frameIdx = 1 : nFlowRows
        Acstr((frameIdx -1) * (nFlowCols - 1) + 1 : ...
            frameIdx * (nFlowCols - 1), (frameIdx -1) * nFlowCols + 1 :...
            frameIdx * nFlowCols) = AsubMat;
    end
    %bcstr = [maxDist * ones((nFlowCols - 1) * nFlowRows, 1)];
    Acstr = [Acstr; -Acstr];
    bcstr = [maxDist * ones((nFlowCols - 1) * nFlowRows, 1); ...
        -minDist * ones((nFlowCols - 1) * nFlowRows, 1)];
    
    % Compute the initial estimate
    tic;
    qpopts = optimset('Algorithm','interior-point-convex');
    cGrid = quadprog(Hqr, fqr, Acstr, bcstr, [], [], [] , [], [], qpopts);
    toc;
    cGrid = reshape(cGrid, [nFlowCols nFlowRows]);
    cGrid = [cFirstFrame; cGrid'];
     
end
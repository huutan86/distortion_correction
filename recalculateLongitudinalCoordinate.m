function tOut = recalculateLongitudinalCoordinate(tIn, prevl)
    % Calculate the new longitudinal coordinate yOut given a longitudinal
    % coordinate yIn in the coordinates of 1 : nFrames when the
    % longitudinal coordinate vector lCord is used to resample the enface.
    % Input(s):
    %   tIn: the longitudinal index in the previous image that was used to
    %   sample either the background or the reference image.
    %   prevl: a vector of the l coordinates estimated from the previous frame.
    % Output(s):
    %   tOut: the longitudinal index in the current frame that corresponse
    %   to the same region.
    % Algorithm:
    %   We have an image of I[prevl(tIn)] or I[tin]. This image is
    %   resampled to generate a corrected image of I1[newl(tout)]. 
    %   The following mappings are performed
    %       -Converts prevl of dimension [Nin x 1] to a new vector:
    %   of newT = [1 2, ...., (prevl[end] - prevl[1]) + 1] with longitudinal
    % coordinates of newL = {prevl[1], prevl[1] + 1, ..., prevl[end]}.
    %       -The tIn-th element of prevl has a longitudinal coordiate of
    %   prevl[tIn]. Therefore, its index can be interpolated as
    %   tOut = interp1(newT, newL, prevl[tIn]);
    
    minL = round(min(prevl));
    maxL = round(max(prevl));
    lIn = prevl(round(tIn));  % Current longitudinal coordinates before resampling.
    %newT =  1 : (maxL - minL)  + 1;
    %newL = linspace(minL, maxL, maxL - minL + 1);  % 
    newT = 1 : length(prevl);
    newL = linspace(minL, maxL, length(prevl));
    tOut = interp1(newT, newL, lIn, 'spline');
end
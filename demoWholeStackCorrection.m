clc;
clear all;
close all;
dataLoc = 'G:\My Drive\Papers\DistortionCorrection\!Structural Reading 2016\Validation_Sets\';  % Data location
dataSets = {'c2','c3', 'c4','c6', 'c7', 'c8', 'c9', 'c10', 'c12','c13','c14','c17',...
    'c18','c19','c21','c22','c23','c24','c27','c29','c31','c33','c36','c37',...
    'c39','c46','c72','c48','c49' 'c50','c54','c56','c65','c66','c67','c70'...
    'c68','c69','c42','c43','c71'};
for idx = 1 : length(dataSets)
    dataSet = dataSets{idx};
    disp(['Processing ' dataSet]);
    outputFolder = strcat(dataLoc, dataSet);
    correctedFolder = strcat(outputFolder, '\corrected\');
    correctedStack = strcat(correctedFolder,'stack\');
    if (~exist(correctedStack))
        mkdir(correctedStack);
        %% Load the cross-sectional stack for correction.
        disp(' Loading the whole stack for correction...');
        fileName=strcat(dataLoc, dataSet, '\!LogIntensityAll.tif');
        InfoImage=imfinfo(fileName);
        numCols=InfoImage(1).Width;
        numRows=InfoImage(1).Height;
        numFrames=length(InfoImage);
        imStack=zeros(numRows, numCols ,numFrames,'single');  % Enface stacks.
        TifLink = Tiff(fileName, 'r');
        for frameIdx=1:numFrames
           if (mod(frameIdx, 100)==0) .
               
               disp(['Reading frame ' num2str(frameIdx) '/' num2str(numFrames)]);
           end
           TifLink.setDirectory(frameIdx);
           imStack(:,:,frameIdx)=single(TifLink.read());
        end
        TifLink.close();
        clear InfoImage;

        %% Parameters to select an area not obstructed by strut.
        firstCircCol = 200;  % First circumferential column
        lastCircCol  = 800;  % Last circumferential column

        % Crop from the 1st to the last columns
        imStack = imStack(:, firstCircCol : lastCircCol, :);

        %% Load the parameters for circumferential and longitudinal correction.
        load(strcat(correctedFolder, 'circumferential_distortion_params.mat'));
        load(strcat(correctedFolder, 'longitudinal_distortion_params.mat'), ...
                'lCoordAllRepeats');

        %% Get the number of columns for the final enface image.
        numCircRepeats = length(cCoordAllRepeats);
        numLongRepeats = length(lCoordAllRepeats);
        for rowIdx = 1 : numRows
            disp(['Working at depth ' num2str(rowIdx)]);
            enfaceIm = squeeze(imStack(rowIdx, :, :))';
            figure(1);
            imshow(normalizeIm(enfaceIm));
            title('Raw input image');
            minEnface = min(enfaceIm(:));
            maxEnface = max(enfaceIm(:));
            % Resample the enface.
            for repeatIdx = 1 : numCircRepeats
                 enfaceIm = resampleEnfaceX(enfaceIm, cCoordAllRepeats{repeatIdx});
            end
            for repeatIdx = 1 : numLongRepeats
                enfaceIm = resampleEnfaceY(enfaceIm, lCoordAllRepeats{repeatIdx});
            end
            figure(2);
            if (rowIdx == 1) 
                imMask = calculateDepaddingMask(enfaceIm);
                newNumCols = size(imMask, 2);
                corrStack = zeros(numRows, newNumCols, numFrames, 'single');
            end
            imshow(normalizeIm(enfaceIm .* imMask, minEnface, maxEnface));
            title('Corrected input image');
            corrStack(rowIdx, :, :) = (enfaceIm .* imMask)';
        end

        % Reslice the stack and save the results
        for frameIdx = 1 : numFrames
            writeTIFF(single(squeeze(corrStack(:, :, frameIdx))),...
                strcat(correctedStack, 'im_', num2str(frameIdx), '.tif'));
        end
    end
end



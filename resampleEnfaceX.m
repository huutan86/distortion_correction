function corrEnface = resampleEnfaceX(enface, cCord)
    % Resample the current frame in the X-dimension.
    % Output:
    %   enface: input enface for correction enface for correction
    %   mask: a binary mask where 1 corresponds to good pixels
    %   F: a linear interpolator that was used for resampling.
    %   xx2, yy2: coordinates of the points 
    % Upsample the X dimension if needed
    nFrames = size(enface, 1);
    %% Calculate the new X coordinates after resampling. These coordinates are xx2 and yy2. Note that there will be points with these coordinate that we don't have info.
    minC = floor(min(cCord(:)));
    maxC = ceil(max(cCord(:)));
    newNCols = maxC - minC + 1;
    xArr2 = linspace(minC, maxC, newNCols);
    cc2 = repmat(xArr2, [nFrames 1]);
    corrEnface = zeros(nFrames, newNCols, 'single');
    for rowIdx = 1 : nFrames
        tempRow = interp1(cCord(rowIdx, :), enface(rowIdx, :), xArr2, 'spline');
        % Repeat the entries for the extrapolated region. We don't want to
        % use `nearest`, `extrap` because this reduces the resolution.
        leftIdx = find(xArr2 < cCord(rowIdx, 1));
        if ~isempty(leftIdx)
            tempRow(leftIdx) = enface(rowIdx, 1);
        end
        rightIdx = find(xArr2 > cCord(rowIdx, end));
        if ~isempty(rightIdx)
            tempRow(rightIdx) = enface(rowIdx, end);
        end
        corrEnface(rowIdx, :) = tempRow;
    end
end

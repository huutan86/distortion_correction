function outIm = normalizeIm(inIm, minVal, maxVal)
% Normalizes the output image to [0, 1]
if nargin < 2
    maxVal = max(inIm(:));
    minVal = min(inIm(:));
end
inIm(inIm > maxVal) = maxVal;
inIm(inIm < minVal) = minVal;
outIm = (inIm - minVal) / (maxVal - minVal);
